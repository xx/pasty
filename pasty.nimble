# Package

version       = "0.1.0"
author        = "Rasmus Moorats"
description   = "Paste service for the Nim playground"
license       = "GPL-3.0-or-later"
srcDir        = "src"
bin           = @["pasty"]


# Dependencies

requires "nim >= 2.0.0"
requires "prologue >= 0.6.6"
requires "db_connector#HEAD"
requires "sqids#HEAD"
