FROM docker.io/nimlang/nim:2.0.2-ubuntu

RUN apt update && apt install -y postgresql-client

WORKDIR /usr/src/app
COPY . /usr/src/app

RUN nimble -y build

ENTRYPOINT ["./pasty"]
