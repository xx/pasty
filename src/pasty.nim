import prologue
import ./[context, env, views]

let settings = newSettings(
  appName = "pasty",
  address = getEnv("PASTY_ADDRESS", "0.0.0.0"),
  port = Port(getEnv("PASTY_PORT", 8080)),
  debug = getEnv("PASTY_DEBUG", false)
)

var app = newApp(settings = settings)

app.addRoute("/", index, HttpGet)
app.addRoute("/", newPaste, HttpPost)
app.addRoute("/{pid}", viewPaste, HttpGet)
app.addRoute("/{pid}", newPasteRevision, HttpPost)
app.addRoute("/{pid}", deletePaste, HttpDelete)
app.addRoute("/{pid}/{revision}", viewPasteRevision, HttpGet)
app.run(PastyContext)
