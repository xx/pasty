import std/[json, strformat, strutils]
import prologue
import ./[consts, context, db, utils]

func origin(ctx: Context): string =
  if ctx.request.hasHeader("origin"): ctx.request.getHeader("origin")[0]
  else: ""

template addDefaultHeaders(ctx: Context): untyped =
  ctx.response.setHeader("Access-Control-Allow-Origin", "*")
  ctx.response.setHeader("Access-Control-Allow-Methods", "POST, GET, DELETE")
  ctx.response.setHeader("Access-Control-Allow-Headers", keyHeader)
  ctx.response.setHeader("X-Content-Type-Options", "nosniff")
  ctx.response.setHeader("Content-Type", "text/plain; charset=utf-8")

template wantsJson(ctx: Context): untyped =
  ctx.request.hasHeader("accept") and prefersJson(ctx.request.getHeader("accept")[0])

template notFound(): untyped =
  await ctx.respond(Http404, "404 Not Found\n", headers = ctx.response.headers)
  ctx.handled = true

template missingBody(): untyped =
  await ctx.respond(Http400, "400 Bad Request\nMissing body value\n", headers = ctx.response.headers)
  ctx.handled = true

template missingKey(): untyped =
  await ctx.respond(Http403, &"403 Forbidden\nMissing {keyHeader} header\n", headers = ctx.response.headers)
  ctx.handled = true

template invalidKey(): untyped =
  await ctx.respond(Http403, &"403 Forbidden\nInvalid access key\n", headers = ctx.response.headers)
  ctx.handled = true

proc index*(ctx: Context) {.async, gcsafe.} =
  ctx.addDefaultHeaders
  await ctx.respond(Http200, indexPage, headers = ctx.response.headers)
  ctx.handled = true

proc newPaste*(ctx: Context) {.async, gcsafe.} =
  ctx.addDefaultHeaders

  let
    ctx = PastyContext(ctx)
    body = ctx.request.body()
  if body.isEmptyOrWhitespace:
    missingBody()
    return

  var pid = genRandomStringSafe(urlLength)
  while ctx.db.snippetWithUrlExists(pid):
    pid = genRandomStringSafe(urlLength)

  let key = genRandomString(32)
  ctx.response.setHeader(keyHeader, key)

  discard ctx.db.insertSnippet(pid, body, key, ctx.origin)

  let url = &"""{ctx.hostname}/{pid}"""
  ctx.response.setHeader("Location", url)

  if ctx.wantsJson:
    resp jsonResponse(%*{"url": url, "key": key}, headers = ctx.response.headers, code = Http201)
  else:
    await ctx.respond(Http201, &"URL: {url}\nKey: {key}\n", headers = ctx.response.headers)
    ctx.handled = true

proc viewPaste*(ctx: Context) {.async, gcsafe.} =
  ctx.addDefaultHeaders

  let ctx = PastyContext(ctx)
  try:
    let (_, _, body) = ctx.db.fetchSnippetByUrl(ctx.getPathParams("pid"))
    await ctx.respond(Http200, body, headers = ctx.response.headers)
    ctx.handled = true
  except NoSuchSnippetError:
    notFound()

proc viewPasteRevision*(ctx: Context) {.async, gcsafe.} =
  ctx.addDefaultHeaders

  let
    ctx = PastyContext(ctx)
    pid = ctx.getPathParams("pid")
  var revision: int
  try:
    revision = ctx.getPathParams("revision").parseInt
  except ValueError:
    notFound()
    return

  try:
    let body = ctx.db.fetchRevisionByUrl(pid, revision)
    await ctx.respond(Http200, body, headers = ctx.response.headers)
    ctx.handled = true
  except NoSuchSnippetError:
    notFound()

proc newPasteRevision*(ctx: Context) {.async, gcsafe.} =
  ctx.addDefaultHeaders

  if not ctx.request.hasHeader(keyHeader):
    missingKey()
    return

  let
    ctx = PastyContext(ctx)
    body = ctx.request.body()
    pid = ctx.getPathParams("pid")

  if body.isEmptyOrWhitespace:
    missingBody()
    return

  try:
    let
      revision = ctx.db.createSnippetRevision(pid, body, ctx.request.getHeader(keyHeader)[0], ctx.origin)
      url = &"{ctx.hostname}/{pid}"
      permalink = &"{url}/{revision}"

    if ctx.wantsJson:
      resp jsonResponse(%*{"url": url, "permalink": permalink}, code = Http303, headers = ctx.response.headers)
    else:
      await ctx.respond(Http201, &"URL: {url}\nPermalink: {permalink}\n", headers = ctx.response.headers)
      ctx.handled = true
  except InvalidAccessKeyError:
    invalidKey()
  except NoSuchSnippetError:
    notFound()

proc deletePaste*(ctx: Context) {.async, gcsafe.} =
  ctx.addDefaultHeaders

  if not ctx.request.hasHeader(keyHeader):
    missingKey()
    return

  let
    ctx = PastyContext(ctx)
    pid = ctx.getPathParams("pid")
  try:
    ctx.db.deleteSnippetByUrl(pid, ctx.request.getHeader(keyHeader)[0])
    await ctx.respond(Http204, "", headers = ctx.response.headers)
    ctx.handled = true
  except InvalidAccessKeyError:
    invalidKey()
  except NoSuchSnippetError:
    notFound()
