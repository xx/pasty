const indexPage* = """Pasty - a free paste service

  Pasty is a minimalistic paste service written in Nim.

  It is designed for the Nim playground, but can be used by anyone.


USAGE

  SUBMIT

    To submit a paste, send a POST request to https://pasty.ee with the paste data in the request body.
    For example, to paste the file 'log.txt' using curl:

      curl --data-binary '@log.txt' https://pasty.ee

    To directly upload command output, you can use '-' as the filename when piping:

      ./my_command | curl --data-binary '@-' https://pasty.ee


  UPDATE

    If you wish, you may update a submitted paste by sending a POST request to a specific snippet's URL.
    To do this, you will need to supply the access key that was shown when first submitting the paste.
    This access key needs to be provided as the value for the header 'X-Pasty-Access-Key'.

    For example, to update the paste at 'https://pasty.ee/examplepaste' using curl:

      curl --data-binary '@file.txt' -H 'X-Pasty-Access-Key: SUPERsecretKEY' https://pasty.ee/examplepaste

    Older revisions are stored, and may be viewed by appending the revision's number to the paste's URL.
    For example, the first submission is always at 'https://pasty.ee/examplepaste/0', the next one is
    at 'https://pasty.ee/examplepaste/1', and so on.


  DELETE

    If you wish to delete a paste, you may send a DELETE request to the paste's URL. As with updating,
    this request requires the correct 'X-Pasty-Access-Key' header value.
    As an example, to delete a paste with curl:

      curl -X DELETE -H 'X-Pasty-Access-Key: SUPERsecretKEY' https://pasty.ee/examplepaste


RULES

  1. No illegal content or links to illegal content.
  2. Don't abuse the service. Submitting individual pastes is fine, but do not integrate this with an
     application you're building.


LINKS

  Source code: https://git.dog/xx/pasty
  Nim playground: https://play.nim-lang.org

CONTACT

  If you've determined illegal content on the service or want to get in touch for partnerships, send
  an email to xx [AT] nns [DOT] ee.
"""

const urlLength* = 8
const keyHeader* = "X-Pasty-Access-Key"
