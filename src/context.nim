import prologue
import db_connector/[db_postgres, postgres]
import ./[env]

type PastyContext* = ref object of Context
   # todo: ideally, we would like to use a pool here
   # the connection pool from norm seems nice
   # for now, 1 connection per thread will do
   db*: DbConn
   hostname*: string

var dbConn {.threadvar.}: DbConn
let hostname = getEnv("PASTY_HOSTNAME", "https://pasty.ee")

method extend*(ctx: PastyContext) {.gcsafe.} =
  if dbConn.isNil:
    dbConn = open(
      getEnv("PASTY_PG_HOST", "localhost"),
      getEnv("PASTY_PG_USER", "pasty"),
      getEnv("PASTY_PG_PASSWORD"),
      getEnv("PASTY_PG_DB", "pasty"),
    )
  elif dbConn.status == CONNECTION_BAD:
    # try to reset in case of network failure
    pqreset(dbConn)
  ctx.db = dbConn
  {.cast(gcsafe).}:
    ctx.hostname = hostname
