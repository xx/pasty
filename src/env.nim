import std/[envvars, strutils]

proc getEnv*(key: string, default: string): string =
  result = envvars.getEnv(key, default = default)

proc getEnv*(key: string): string =
  result = envvars.getEnv(key)
  if result == "":
    raise newException(Exception, "Environment variable " & key & " is unset!")

proc getEnv*(key: string, default: int): int =
  let val = envvars.getEnv key
  result =
    if val == "": default
    else: parseInt val

proc getEnv*(key: string, default: bool): bool =
  let val = envvars.getEnv key
  result = val.toLower in @["1", "y", "yes", "true", "on", "enable", "enabled"]
