import std/[random, sets, strutils, sysrand]
import sqids/defaults # for the blocklist

# truly randomize
var buffer = newSeq[uint8](8)
if urandom(buffer):
  var seed = 0'i64
  for i in 0..<8:
    seed = seed or (buffer[i].int64) shl (i * 8)
  randomize(seed)
else:
  echo "Failed to randomize using sysrand! Falling back to timestamp"
  randomize()

proc genRandomString*(n: int): string =
  ## Generates a random string for URLs and secret keys.
  result = newStringOfCap(n)
  for i in 0..<n:
    result.add(Letters.sample)

proc isDecimal(x: string): bool =
  try:
    discard parseInt(x)
    result = true
  except ValueError:
    result = false

proc isInBlocklist(input: string): bool =
  ## This function is pretty much copied from the sqids library.
  let id = input.toLower

  for word in defaultBlocklist:
    if word.len <= id.len:
      if id.len <= 3 or word.len <= 3:
        if id == word:
          return true
      elif word.isDecimal():
        if id.startsWith(word) or id.endsWith(word):
          return true
      elif word in id:
        return true

  return false

proc genRandomStringSafe*(n: int): string =
  ## Like genRandomString, but additionally checks for vulgarity.
  ## It uses the sqids blocklist so we don't actually include it in
  ## our project.
  result = genRandomString(n)
  while result.isInBlocklist:
    result = genRandomString(n)

func prefersJson*(accept: string): bool =
  ## Inspects the HTTP Accept header to determine whether the clients
  ## prefers to receive a response in JSON instead of plaintext.
  # todo: look at the q-value too
  let accepted = accept.split(",")

  for mime in accepted:
    let subtypes = mime.split("/")
    if subtypes.len < 2:
      # deformed
      continue
    case subtypes[1].split(";")[0].toLower
    of "plain": return false
    of "json": return true

  return false

when isMainModule:
  # some tests
  doAssert prefersJson("application/json;q=1.0, text/plain")
  doAssert (not prefersJson("text/plain;q=1.0; application/json;q=0.9"))
  doAssert (not prefersJson("*/*"))
  doAssert (not prefersJson(""))
